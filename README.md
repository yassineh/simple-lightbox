# simple-lightbox (coding exercise)

## Live Demo

A live demo can be seen here: http://projects.webmy.me/simple-lightbox/dist/#/

## Exercise Rules

This exercise is meant to demonstrate:

* The ability to access to a public API and successfully retrieve data from it;
* The ability to display that data on a page using only native JavaScript (no libraries); and
* The ability to update the UI of a page without refreshing the page.

I'd like you to create a web page that shows a photo in a lightbox view, with the ability to move to the next / previous photos and display the photo title. You can use any public API that returns photos; here are some ideas:

* Flickr: https://www.flickr.com/services/api/flickr.photosets.getPhotos.html
* Instagram: http://instagram.com/developer/
* Google Image Search: https://developers.google.com/custom-search/json-api/v1/overview
* Yahoo Image Search: https://developer.yahoo.com/boss/search/

We don't expect this will take more than three hours, but submitting something you're proud of is far more important than any amount of time spent or not spent on the challenge. Make sure you're happy with the result! It should run without errors in the latest versions of Chrome, Safari, Firefox and IE.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.