'use strict';
/**
 * @ngdoc overview
 * @name simpleLightboxApp
 * @description
 * # simpleLightboxApp
 *
 * Main module of the application.
 */
angular
    .module('simpleLightboxApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/flickr.html',
                controller: 'FlickrCtrl',
                controllerAs: 'flickr'
            })
            .when('/flickr', {
                templateUrl: 'views/flickr.html',
                controller: 'FlickrCtrl',
                controllerAs: 'flickr'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
