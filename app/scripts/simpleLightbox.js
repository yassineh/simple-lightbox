'use strict';

var simpleLightbox = simpleLightbox || {};

simpleLightbox.controller = {
    'currentPicIndex' : -1,
    'niceCollections' : [
        {id: '72157655252887952', name: 'His personal favorites'},
        {id: '72157645368119960', name: 'Landscapes'},
        {id: '72157645309380618', name: 'I\'m a wild one'},
        {id: '72157645699458424', name: '...moments...'},
        {id: '72157640184445595', name: 'Dubai'}
    ],
    'lightbox': {},
    'currentCollection': {},
    'init': function(){
        this.lightbox = document.querySelector('#lightbox');
        this.generateSuggestions();
        this.loadCollection(simpleLightbox.controller.niceCollections[0].id);
    },
    'generateSuggestions': function(){
        var container = document.querySelector('#suggestionInput');
        this.niceCollections.forEach(function(element, index){
            var node = document.createElement('option');
            node.setAttribute('value', element.id);
            node.innerHTML = element.name;
            container.appendChild(node);
        });
    },
    'isLoading': function(isLoading){
        if (isLoading){
            document.querySelector('.loading-indicator').setAttribute('class', 'loading-indicator');
        } else {
            document.querySelector('.loading-indicator').setAttribute('class', 'loading-indicator hidden');
        }
    },
    'showWarning': function(message){
        var warningConsole = document.querySelector('.warning-message');
        warningConsole.innerHTML(message);
        if (message)
            warningConsole.setAttribute('class', 'warning-message');
        else
            warningConsole.setAttribute('class', 'warning-message hidden');
    },
    'loadCollection': function(collectionId){
        if (!collectionId){
            var container = document.querySelector('#suggestionInput');
            collectionId = container.options[container.selectedIndex].value;
        }
        this.isLoading(true);
        var xmlhttp = simpleLightbox.flickrService.getCollectionInfo(collectionId);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4){
                simpleLightbox.controller.isLoading(false);
                if (xmlhttp.status == 200) {
                    var data = JSON.parse(xmlhttp.responseText);
                    if (data.photoset){
                        simpleLightbox.controller.currentCollection = data.photoset;
                        simpleLightbox.controller.generateThumbnails();
                    }
                } else {
                    console.log('Something went wrong with the API call.');
                    simpleLightbox.controller.showWarning('Something went wrong');
                }
            }
        };
    },
    'refreshLightbox' : function (desiredIndex) {
        simpleLightbox.controller.currentPicIndex = parseInt(desiredIndex);
        var lightBoxImg = document.querySelector('#lightbox .img-responsive');
        var imgTitle = document.querySelector('#lightbox .img-title');
        var selectedPhoto = simpleLightbox.controller.currentCollection.photo[parseInt(simpleLightbox.controller.currentPicIndex)];
        if (selectedPhoto){
            lightBoxImg.setAttribute('src', simpleLightbox.flickrService.buildUrlFromObject(selectedPhoto, 'b'));
            imgTitle.innerHTML = selectedPhoto.title;
        }

    },
    'getPreviousPic' : function () {
        simpleLightbox.controller.refreshLightbox((simpleLightbox.controller.currentPicIndex > 0) ? parseInt(simpleLightbox.controller.currentPicIndex) - 1 : simpleLightbox.controller.currentCollection.photo.length - 1);
    },
    'getNextPic' : function () {
        simpleLightbox.controller.refreshLightbox((simpleLightbox.controller.currentPicIndex < simpleLightbox.controller.currentCollection.photo.length - 1) ? parseInt(simpleLightbox.controller.currentPicIndex) + 1 : 0);
    },
    'generateThumbnails': function () {
        var container = document.querySelector('.imageset');
        if (!container) {
            console.log('cannot find container');
            return;
        }
        container.innerHTML = '';
        simpleLightbox.controller.currentCollection.photo.forEach(function (element, index) {
            var imageContainer = document.createElement('div');
            var imageThumbnail = document.createElement('div');
            imageContainer.setAttribute('class', 'col-sm-4 single-photo-container');
            imageThumbnail.setAttribute('data-photo-index', index);
            imageThumbnail.setAttribute('class', 'single-photo-subcontainer');
            imageThumbnail.style.backgroundImage = 'url(' + simpleLightbox.flickrService.buildUrlFromObject(element) + ')';
            imageThumbnail.addEventListener("click", function (event) {
                simpleLightbox.controller.thumbnailClick(event);
                //$('#lightbox').modal('show');
            });
            imageContainer.appendChild(imageThumbnail);
            container.appendChild(imageContainer);
        })
    },
    'thumbnailClick': function(event){
        simpleLightbox.controller.refreshLightbox(parseInt(event.target.getAttribute('data-photo-index')));
        simpleLightbox.controller.showLightbox();
    },
    'showLightbox': function(){
        this.lightbox.setAttribute('class', '');
    },
    'hideLightbox': function(){
        this.lightbox.setAttribute('class', 'hidden');
    }
};

simpleLightbox.util = {
     'encodeQueryData': function(data) {
            var ret = [];
            for (var d in data)
                ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
            return ret.join("&");
        }
};

simpleLightbox.flickrService = {
    'apiKey': '0861ce4e45a2d1750dd6a5599cbbbf1b',
    'apiBaseUrl': 'https://api.flickr.com/services/rest?',
    'getApiUrl': function(collectionId){
        var parameters = {
            'method': 'flickr.photosets.getPhotos',
            'api_key': simpleLightbox.flickrService.apiKey,
            'photoset_id': collectionId,
            'format': 'json',
            'nojsoncallback': 1
        };
        return this.apiBaseUrl + simpleLightbox.util.encodeQueryData(parameters);
    },
    'getCollectionInfo': function(collectionId){
        var apiUrl = this.getApiUrl(collectionId);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", apiUrl, true);
        xmlhttp.send();
        return xmlhttp;
    },
    'buildUrlFromObject': function(object, size){
        if (object === undefined || !object.id)
            return;
        if (!size)
            size = 'n';
        return 'https://farm' + object.farm + '.staticflickr.com/'
            + object.server + '/' + object.id + '_' + object.secret + '_' + size + '.jpg';
    }
};