'use strict';
function initApp(){
    if (simpleLightbox && simpleLightbox.controller)
        simpleLightbox.controller.init();
}
document.addEventListener('DOMContentLoaded', initApp, false);

