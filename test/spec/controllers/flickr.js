'use strict';

describe('Controller: FlickrCtrl', function () {

  // load the controller's module
  beforeEach(module('simpleLightboxApp'));

  var FlickrCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FlickrCtrl = $controller('FlickrCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FlickrCtrl.awesomeThings.length).toBe(3);
  });
});
